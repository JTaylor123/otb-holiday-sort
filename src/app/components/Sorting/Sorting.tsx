'use client'
import { Sort } from "@/app/types";
import styles from "./Sorting.module.scss";

type SortingProps = {
    onChangeSort: (sort: Sort) => void;
    sort: Sort;
}

function Sorting({onChangeSort, sort}: SortingProps) {
  
    return (
      <div>
        <div className={styles.formGroup}>
            <input
            type="radio"
            id="alphapbetically"
            name="sortOptions"
            checked={sort === Sort.Name}
            onChange={() => onChangeSort(Sort.Name)}
            />
            <label htmlFor="alphapbetically">
                sort&nbsp;<b>alphabetically</b>
                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" fill="currentColor">
                    <path fillRule="evenodd" d="M3.792 2.938A49.069 49.069 0 0 1 12 2.25c2.797 0 5.54.236 8.209.688a1.857 1.857 0 0 1 1.541 1.836v1.044a3 3 0 0 1-.879 2.121l-6.182 6.182a1.5 1.5 0 0 0-.439 1.061v2.927a3 3 0 0 1-1.658 2.684l-1.757.878A.75.75 0 0 1 9.75 21v-5.818a1.5 1.5 0 0 0-.44-1.06L3.13 7.938a3 3 0 0 1-.879-2.121V4.774c0-.897.64-1.683 1.542-1.836Z" clipRule="evenodd" />
                </svg>
            </label>
        </div>
        <div className={styles.formGroup}>
            <input
            type="radio"
            id="price"
            name="sortOptions"
            checked={sort === Sort.Price}
            onChange={() => onChangeSort(Sort.Price)}
            />
            <label htmlFor="price">
                sort by&nbsp;<b>price</b>
                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" fill="currentColor">
                    <path fillRule="evenodd" d="M12 2.25c-5.385 0-9.75 4.365-9.75 9.75s4.365 9.75 9.75 9.75 9.75-4.365 9.75-9.75S17.385 2.25 12 2.25ZM9.763 9.51a2.25 2.25 0 0 1 3.828-1.351.75.75 0 0 0 1.06-1.06 3.75 3.75 0 0 0-6.38 2.252c-.033.307 0 .595.032.822l.154 1.077H8.25a.75.75 0 0 0 0 1.5h.421l.138.964a3.75 3.75 0 0 1-.358 2.208l-.122.242a.75.75 0 0 0 .908 1.047l1.539-.512a1.5 1.5 0 0 1 .948 0l.655.218a3 3 0 0 0 2.29-.163l.666-.333a.75.75 0 1 0-.67-1.342l-.667.333a1.5 1.5 0 0 1-1.145.082l-.654-.218a3 3 0 0 0-1.898 0l-.06.02a5.25 5.25 0 0 0 .053-1.794l-.108-.752H12a.75.75 0 0 0 0-1.5H9.972l-.184-1.29a1.863 1.863 0 0 1-.025-.45Z" clipRule="evenodd" />
                </svg>
            </label>
        </div>
        <div className={styles.formGroup}>
            <input
            type="radio"
            id="rating"
            name="sortOptions"
            checked={sort === Sort.Rating}
            onChange={() => onChangeSort(Sort.Rating)}
            />
            <label htmlFor="rating">
                sort by&nbsp;<b>rating</b>
                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" fill="currentColor">
                    <path fillRule="evenodd" d="M10.788 3.21c.448-1.077 1.976-1.077 2.424 0l2.082 5.006 5.404.434c1.164.093 1.636 1.545.749 2.305l-4.117 3.527 1.257 5.273c.271 1.136-.964 2.033-1.96 1.425L12 18.354 7.373 21.18c-.996.608-2.231-.29-1.96-1.425l1.257-5.273-4.117-3.527c-.887-.76-.415-2.212.749-2.305l5.404-.434 2.082-5.005Z" clipRule="evenodd" />
                </svg>
            </label>
        </div>
      </div>
    )
}

export default Sorting
