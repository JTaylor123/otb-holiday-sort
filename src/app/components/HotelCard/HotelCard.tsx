'use client'
import Image from "next/image";
import styles from "./HotelCard.module.scss";
import StarRating from "../StarRating/StarRating";
import { useState } from 'react';

type HotelCardProps = {
    image: string;
    name: string;
    location: string;
    rating: number;
    adults: number;
    children: number;
    infants: number;
    startDate: string;
    duration: number;
    airport: string;
    price: number;
    extraInfo: string;
}

function displayPersons(adults: number, children: number, infants: number): string {
    const formatPerson = (count: number, label: string, pluralLabel: string) => {
        return count !== 0 ? `${count} ${count !== 1 ? pluralLabel : label}` : '';
    };
    
    const adultText = formatPerson(adults, "adult", "adults");
    const childText = formatPerson(children, "child", "children");
    const infantText = formatPerson(infants, "infant", "infants");
    
    const texts = [adultText, childText, infantText].filter(Boolean);
    
    let result = '';
    
    if (texts.length === 1) {
        result = texts[0];
    } else if (texts.length === 2) {
        result = texts.join(", ");
    } else if (texts.length === 3) {
        result = `${texts.slice(0, -1).join(", ")}, & ${texts[texts.length - 1]}`;
    }
    
    return result;
}


function HotelCard(props: HotelCardProps) {
    const [showExtraInfo, setShowExtraInfo] = useState(false);

    const toggleExtraInfo = () => {
        setShowExtraInfo(!showExtraInfo);
    };
    
  return (
    <div className={styles.card}>
        <div className={styles.image}>
            <Image
            className={styles.cover}
            src={props.image}
            width={1600}
            height={900}
            alt="hotel-1"
            />
            <button className={styles.toggle} onClick={toggleExtraInfo}>
                <b>{showExtraInfo ? 'Read less' : 'Read more'}</b> &nbsp;about this hotel
                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" className={`${styles.toggleIcon} ${showExtraInfo ? styles.active : ''}`}>
                    <path fillRule="evenodd" d="M12.53 16.28a.75.75 0 0 1-1.06 0l-7.5-7.5a.75.75 0 0 1 1.06-1.06L12 14.69l6.97-6.97a.75.75 0 1 1 1.06 1.06l-7.5 7.5Z" clipRule="evenodd" />
                </svg>
            </button>
        </div>
         
        <div className={styles.details}>
            <article>
                <p className={styles.title}>{props.name}</p>
                <p className={styles.location}>{props.location}</p>
                <StarRating rating={props.rating} />
                <p className={styles.extra}>
                    <b>{displayPersons(props.adults, props.children, props.infants)}</b>
                    <br/>
                    <b>{props.startDate}</b> for <b>{props.duration} days</b> 
                    <br/>
                    departing from <b>{props.airport}</b>
                </p>
            </article>
            <a className={styles.btn} href="#">
                <p className={styles.small}>Book Now</p>
                <p className={styles.large}>
                    {new Intl.NumberFormat('en-GB', { style: 'currency', currency: 'GBP' }).format(props.price)}
                </p>
            </a>
        </div>

        {showExtraInfo && (
            <div className={styles.extraInformation}>
                <p className={styles.subtitle}>Overview</p>
                <p>{props.extraInfo}</p>
            </div>
        )}
    </div>
  )
}

export default HotelCard