
export const hotels = [
    {
      image: "/assets/hotel-image-1.png",
      name: "Iberostar Grand Salome",
      location: "Costa Adeje, Tenerife",
      rating: 5,
      adults: 2,
      children: 2,
      infants: 1,
      startDate: "3rd July 2019",
      duration: 7,
      airport: "East Midlands",
      price: 1136.50,
      extraInfo: "The Iberostar Grand Salome has an exceptional location in the south of Tenerife, overlooking the Atlantic Ocean. It is situated between the Goi del sur and the Amario Goi courses, and is an laeal notel Tor Tamilles, couples and groups who are looking for a nollday Tull or sport, sun and sea"
    },
    {
      image: "/assets/hotel-image-2.png",
      name: "Aguamarina Golf Hotel",
      location: "Costa Adeje, Tenerife",
      rating: 4,
      adults: 2,
      children: 1,
      infants: 0,
      startDate: "27th May 2019",
      duration: 7,
      airport: "Liverpool",
      price: 696.80,
      extraInfo: "The Iberostar Grand Salome has an exceptional location in the south of Tenerife, overlooking the Atlantic Ocean. It is situated between the Goi del sur and the Amario Goi courses, and is an laeal notel Tor Tamilles, couples and groups who are looking for a nollday Tull or sport, sun and sea"
    },
    {
      image: "/assets/hotel-image-3.png",
      name: "Las Pirimides Resort",
      location: "Costa Adeje, Tenerife",
      rating: 3,
      adults: 2,
      children: 2,
      infants: 0,
      startDate: "3rd July 2019",
      duration: 7,
      airport: "Manchester",
      price: 499.99,
      extraInfo: "The Iberostar Grand Salome has an exceptional location in the south of Tenerife, overlooking the Atlantic Ocean. It is situated between the Goi del sur and the Amario Goi courses, and is an laeal notel Tor Tamilles, couples and groups who are looking for a nollday Tull or sport, sun and sea"
    },
];

