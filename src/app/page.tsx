'use client'
import Image from "next/image";
import styles from "./page.module.scss";
import HotelCard from "./components/HotelCard/HotelCard";
import Sorting from "./components/Sorting/Sorting";
import { useCallback, useMemo, useState } from "react";
import { hotels } from "./hotels";
import { Sort } from "./types";

export default function Home() {
  const [sortBy, setSortBy] = useState(Sort.Price);

  const handleSortChange = useCallback((sortBy: Sort) => {
    setSortBy(sortBy);
  },[setSortBy]);

  const sortedHotels = useMemo(() => hotels.sort((a, b) => {
    switch (sortBy) {
      case Sort.Price:
        return a.price - b.price;
      case Sort.Name:
        return a.name.localeCompare(b.name);
      case Sort.Rating:
        return b.rating - a.rating;
    }
  }),[sortBy]);

  return (
    
    <main className={styles.main}>
      <div className={styles.results}>
        <div className={styles.filters}>
          <div className={styles.container}>
            <Sorting onChangeSort={handleSortChange} sort={sortBy}/>
          </div>
        </div>
        <div className={styles.filteredResults}>
          <div className={styles.container}>
            {sortedHotels.map((hotel) => (
              <HotelCard key={hotel.name} {...hotel} />
            ))}
          </div>
        </div>
      </div>
    </main>
  );
}
